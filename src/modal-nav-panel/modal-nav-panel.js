export default class ModalNavPanel extends HTMLElement {
  constructor() {
    super();

    this.shadowObj = this.attachShadow({
      mode: 'open'
    });
  }

  connectedCallback() {
    this.render();

    this.button = this.shadowObj.querySelector('.trigger');

    this.handleButtonClick = this.handleButtonClick.bind(this);

    this.shadowRoot.addEventListener('click', this.handleButtonClick);

  }

  disconnectedCallback() {
    this.shadowRoot.removeEventListener('click', this.handleButtonClick);
  }

  handleButtonClick(e) {
    const modalWrapper = e.target.closest('[isopen]');
    if(modalWrapper) {
      e.stopImmediatePropagation();
      modalWrapper.removeAttribute('isopen');
    }
  }

  render() {
    this.shadowObj.innerHTML = this.getTemplate();
  }

  getTemplate() {
    return `
    <style>
    :host {
      display: block;
    }

    </style>
    <div class="modal-menu">
      <slot name="close-button" class="trigger"></slot>
      <slot name="panel"></slot>
    </div>
    `;
  }

}
