export default class ModalNav extends HTMLElement {
  constructor() {
    super();

    this.shadowObj = this.attachShadow({
      mode: 'open'
    });
  }

  connectedCallback() {
    this.render();

    this.button = this.shadowObj.querySelector('.trigger');
    this.menu = this.shadowObj.querySelector('.modal-menu');
    this.focusables = null;
    this.openClass = this.getAttribute('openClass');

    this.handleOpenButtonClick = this.handleOpenButtonClick.bind(this);
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.handleEscape = this.handleEscape.bind(this);
    this.handleCycleFocus = this.handleCycleFocus.bind(this);
    this.getFocusables = this.getFocusables.bind(this);

    this.shadowRoot.addEventListener('click', this.handleOpenButtonClick);

    this.openedEvent = new CustomEvent('bomber-modal-nav-opened', {
      detail: {
        isOpen: true
      }
    });

    this.closedEvent = new CustomEvent('bomber-modal-nav-closed', {
      detail: {
        isOpen: false
      }
    });

  }

  disconnectedCallback() {
    document.removeEventListener('click', this.handleMenuClick);
    this.shadowRoot.removeEventListener('click', this.handleOpenButtonClick);
    window.removeEventListener('keyup', this.handleEscape);
    window.removeEventListener('keydown', this.handleCycleFocus);

    if(this.openClass) {
      document.body.classList.remove(this.openClass);
    }
  }

  static get observedAttributes() {
    return ['isopen'];
  }

  attributeChangedCallback(name, oldval, newval) {
    if (name === 'isopen') {
      this.isopen = newval;
    }
  }

  // All observed attributes should have a corresponding private property.
  // See https://robdodson.me/posts/interoperable-custom-elements/
  set isopen(value) {
    if (this._isopen === value) {
      return;
    }
    this._isopen = value;

    if(this._isopen !== null) {
      this.openMenu();
    } else {
      this.closeMenu();
    }
  }

  get isopen() {
    return this._isopen;
  }

  openMenu() {
    this.menu.classList.add('open');
    if(this.openClass) {
      document.body.classList.add(this.openClass);
    }
    document.addEventListener('click', this.handleMenuClick);
    document.dispatchEvent(this.openedEvent);
    this.button.removeEventListener('click', this.handleOpenButtonClick);
    window.addEventListener('keyup', this.handleEscape);
    if(!this.focusables) {
      this.focusables = this.getFocusables();
    }
    window.addEventListener('keydown', this.handleCycleFocus);
  }

  closeMenu() {
    this.menu.classList.remove('open');
    if(this.openClass) {
      document.body.classList.remove(this.openClass);
    }
    document.removeEventListener('click', this.handleMenuClick);
    document.dispatchEvent(this.closedEvent);
    this.button.addEventListener('click', this.handleOpenButtonClick);
    window.removeEventListener('keyup', this.handleEscape);
    window.removeEventListener('keydown', this.handleCycleFocus);
  }

  handleOpenButtonClick(e) {
    if(e.target.closest('[slot="open-button"]')) {
      e.stopImmediatePropagation();
      this.setAttribute('isopen', '');
    }
  }

  handleMenuClick(e) {
    if (e.target.closest('[slot="modal"]')) {
      if(e.target.closest('a')) {
        console.log(e.target.closest('a').href);
      }
    } else {
      this.removeAttribute('isopen');
    }
  }

  handleEscape(e) {
    if (e.key === 'Escape' || e.keyCode === 27) {
      e.preventDefault();
      this.removeAttribute('isopen');
    }
  }

  handleCycleFocus(e) {
    if (!this.focusables) {
      return;
    }

    const KEYCODE_TAB = 9;
    const isTabPressed = e.key === 'Tab' || e.keyCode === KEYCODE_TAB;
    const firstFocusable = this.focusables[0];
    const lastFocusable = this.focusables[this.focusables.length - 1];

    if (!isTabPressed) {
      return;
    }

    if (e.shiftKey) {
      if (document.activeElement === firstFocusable) {
        lastFocusable.focus();
        e.preventDefault();
      }
    } else {
      if (document.activeElement === lastFocusable) {
        firstFocusable.focus();
        e.preventDefault();
      }
    }
  };

  render() {
    this.shadowObj.innerHTML = this.getTemplate();
  }

  getFocusables() {
    return this.querySelectorAll('a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])');
  };

  getTemplate() {
    return `
    <style>
    :host {
      display: block;
    }

    .modal-menu {
      display: none;
    }

    .modal-menu.open {
      background: var(--bomber-modal-nav-overlay-bg, #ffffff);
      display: block;
      position: fixed;
      top: 0;
      left: 0;
      height: 100vh;
      width: 100vw;
      overflow: auto;
    }

    ${this.getAttribute('disableWhenMatches') ? `
    @media ${this.getAttribute('disableWhenMatches')} {
      .modal-menu {
        display: block;
      }

      .modal-menu.open {
        background: none;
        position: static;
        height: auto;
        width: auto;
      }
    }
    ` : ''}
    </style>
    <div class="toggle">
      <slot class="trigger" name="open-button"></slot>
      <div class="modal-menu">
        <slot name="modal" class="modal-slot"></slot>
      </div>
    </div>
    `;
  }

}
