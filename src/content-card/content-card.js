export default class ContentCard extends HTMLElement {
  constructor() {
    super();

    this.shadowObj = this.attachShadow({
      mode: 'open'
    });
  }

  connectedCallback() {
    this.render();
  }

  disconnectedCallback() {
    this.shadowRoot.removeEventListener('click', this.handleButtonClick);
  }


  static get observedAttributes() {
    return ['accentcolor'];
  }

  attributeChangedCallback(name, oldval, newval) {
    if (name === 'accentcolor') {
      this.accentcolor = newval;
    }
  }

  // All observed attributes should have a corresponding private property.
  // See https://robdodson.me/posts/interoperable-custom-elements/
  set accentcolor(value) {
    if (this._accentcolor === value) {
      return;
    }
    this._accentcolor = value;

    this.render();
  }

  get accentcolor() {
    return this._accentcolor;
  }

  render() {
    this.shadowObj.innerHTML = this.getTemplate();
  }

  getTemplate() {
    return `
    <style>
    :host {
      display: block;
    }

    .card::before {
      background: ${this.getAttribute('accentcolor') ?
        this.getAttribute('accentcolor') : 'var(--bomber-content-card-accent-color, #000)'
      };
      border-radius: var(--bomber-content-card-accent-radius, 10px);
      content: '';
      display: block;
      height: var(--bomber-content-card-accent-height, 5px);
      width: var(--bomber-content-card-accent-width, 50px);
    }
    </style>
    <div class="card">
      <slot></slot>
    </div>
    `;
  }

}
